<form class="navbar-form pull-right" action='/search.php' method='get'>
	<input class="span2" name='search_term' type='text' size="10" value='{$search_term|escape:'html':'UTF-8'}'/>
	<select class="span2" name='search_type'>
		<option value='artist' {if $search_type == 'artist'}selected{/if}>{t}Artist{/t}</option>
		<option value='user' {if $search_type == 'user'}selected{/if}>{t}User{/t}</option>
		<option value='tag' {if $search_type == 'tag'}selected{/if}>{t}Tag{/t}</option>
	</select>
	<button class="btn" type='submit' id='search_button'>{t}Search{/t}</button>
</form>
