{include file='header.tpl' subheader='user-header.tpl' showbio=true}

{if $nowplaying}
<div class="alert alert-info">
	<strong>Now playing: </strong> <a href="{$nowplaying[0].trackurl}">{$nowplaying[0].track}</a> by <a href="{$nowplaying[0].artisturl}">{$nowplaying[0].artist}</a>
</div>
{/if}

{if $scrobbles}
<h4>Recent plays</h4>
{include file="tracklist.tpl" class=#table# items=$scrobbles fstream=true fartist=true flove=true ftime=true}
{/if}

{if !empty($lovedArtists)}
	<br />
	<h3>{t name=$me->name}Free artists that %1 loves{/t}</h3>
	<ul class="tagcloud">
		{section name=i loop=$lovedArtists}
			<li style='font-size:{$lovedArtists[i].size}'><a href='{$lovedArtists[i].pageurl}'>{$lovedArtists[i].name}</a></li>
		{/section}
	</ul>
{/if}

{if !empty($recommendedArtists)}
	<br />
	<h3>{t name=$me->name}Free artists that %1 might like{/t}</h3>
	<ul class="tagcloud">
		{foreach from=$recommendedArtists item=artist}
			<li style='font-size:{$artist.size}'><a href='{$artist.url}'>{$artist.artist}</a></li>
		{/foreach}
	</ul>
{/if}

{include file='footer.tpl'}
