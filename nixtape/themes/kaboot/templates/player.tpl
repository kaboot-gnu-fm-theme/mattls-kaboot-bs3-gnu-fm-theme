<div id="player">

	<audio id="audio" autobuffer>
		{if $track->streamurl}
			<object id="fallbackembed" type="application/ogg" data="{$track->streamurl}"><a type="application/ogg" rel="enclosure" href="{$track->streamurl}">Listen to this track</a></object>
		{/if}
	</audio>

	<p id="loading"></p>
	<div class="player-panel" id="interface">
		<div id="infobox">
			<span id="trackinfo"><span id="artistname"></span>
				<div class="pull-right" id="timeinfo">
					<span id="currenttime"></span>/<span id="duration"></span>
				</div>
				<br />
				<span id="trackname"></span><span class="label label-info pull-right" id="scrobbled" style="display:none;">Scrobbled</span>
			</span>
			<div id="tracktags">
				<ul>
				</ul>
			</div>
		</div>
		
		<div id="progress">
			<div id="progress-slider" title="Seek to time"></div>
		</div>
	
		{* Button groups have display bugs when hiding first/last buttons with display:none*}
		<div class="btn-toolbar" id="p_buttons">
			<div class="btn-group">
{*				<a class="btn btn-mini" id="toggleplay"><i class="icon-play" alt="{t}Play{/t}" title="{t}Play{/t}"></i></a>*}
				<a class="btn btn-mini" id="play"><i class="icon-play" alt="{t}Play{/t}" title="{t}Play{/t}"></i></a>
				<a class="btn btn-mini" id="pause"><i class="icon-pause" alt="{t}Pause{/t}" title="{t}Pause{/t}"></i></a>
				<a class="btn btn-mini" id="skipback"><i class="icon-step-backward" alt="{t}Skip Backwards{/t}" title="{t}Skip Backwards{/t}"></i></a>
				<a class="btn btn-mini" id="skipforward"><i class="icon-step-forward" alt="{t}Skip Forwards{/t}" title="{t}Skip Forwards{/t}"></i></a>
			</div>
			<div  class="btn-group">
			{if $logged_in}
				<a class="btn btn-mini" id="ban"><i class="icon-ban-circle" alt="{t}Ban{/t}" title="{t}Ban{/t}"></i></a>
{*				<a class="btn btn-mini" id="toggletag"><i class="icon-tag" alt="{t}Tag{/t}" title="{t}Tag{/t}"></i></a>*}
				<a class="btn btn-mini" id="open_tag"><i class="icon-tag" alt="{t}Tag{/t}" title="{t}Tag{/t}"></i></a>
				<a class="btn btn-mini" id="close_tag"><i class="icon-tag" alt="{t}Tag{/t}" title="{t}Tag{/t}"></i></a>
				<a class="btn btn-mini" id="love"><i class="icon-heart" alt="{t}Love{/t}" title="{t}Love{/t}"></i></a>
			{/if}
			</div>
			<div class="btn-group pull-right">
				<a class="btn btn-mini" id="volume"><i class="icon-volume-up" alt="{t}Volume{/t}" title="{t}Volume{/t}"></i></a>
{*				<a class="btn btn-mini" id="toggleplaylist"><i class="icon-list" alt="{t}Playlist{/t}" title="{t}Playlist{/t}"></i></a>*}
				<a class="btn btn-mini" id="hideplaylist"><i class="icon-list" alt="{t}Playlist{/t}" title="{t}Playlist{/t}"></i></a>
				<a class="btn btn-mini" id="showplaylist"><i class="icon-list" alt="{t}Playlist{/t}" title="{t}Playlist{/t}"></i></a>

			</div>
		</div>
	
		<div id="volume-box">
			<div id="volume-slider"></div>
		</div>
		
		<div id="tagbox">
			<form id="tag_input">
				<span>{t}Tag this track:{/t}</span>
				<div class="input-append">
					<input type='text' id='tags' name='tags' placeholder='guitar, violin, female vocals'/>
					<button class='btn btn-small' id='tag_button'>{t}Tag{/t}</button>
				</div>
			</form>
		</div>

		<div id="playlist">
			<ul class="unstyled" id="songs">
			</ul>
		</div>
	</div>
	<br />
{*	<span id="toggleproblems">{t}Player problems?{/t}</span>*}
	<div id="problems">
		<p>{t escape=no}The player currently works in <a href='http://www.chromium.org/Home'>Chromium</a> and <a href='http://www.gnu.org/software/gnuzilla/'>Icecat</a>/<a href='http://www.mozilla.org/en/firefox/'>Firefox</a> 3.5 or later &mdash; it may also work in Chrome and Opera, though we don't recommend them.{/t}</p>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {ldelim}
		{if $station == 'track'}
			var playlist = [{ldelim}"artist" : "{$track->artist_name|escape:'javascript'}", "album" : "{$track->album_name|escape:'javascript'}", "track" : "{$track->name|escape:'javascript'}", "url" : "{$track->streamurl}"{rdelim}];
			var radio_session = false;
			var station = false;
		{else}
			var playlist = false;
			var radio_session = "{$radio_session}";
			var station = "{$station}";
		{/if}
		{if isset($this_user)}
		playerInit(playlist, "{$this_user->getScrobbleSession}", "{$this_user->getWebServiceSession()}", false, station);
		{else}
		playerInit(playlist, false, false, radio_session, false);
		{/if}
	{rdelim});
</script>
